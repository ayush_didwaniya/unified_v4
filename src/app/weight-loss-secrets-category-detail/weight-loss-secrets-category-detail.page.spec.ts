import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeightLossSecretsCategoryDetailPage } from './weight-loss-secrets-category-detail.page';

describe('WeightLossSecretsCategoryDetailPage', () => {
  let component: WeightLossSecretsCategoryDetailPage;
  let fixture: ComponentFixture<WeightLossSecretsCategoryDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeightLossSecretsCategoryDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeightLossSecretsCategoryDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
