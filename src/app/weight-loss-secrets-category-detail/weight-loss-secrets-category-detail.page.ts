import { Component, OnInit, ViewChild } from '@angular/core';
import { Slides } from '@ionic/angular';

@Component({
  selector: 'app-weight-loss-secrets-category-detail',
  templateUrl: './weight-loss-secrets-category-detail.page.html',
  styleUrls: ['./weight-loss-secrets-category-detail.page.scss'],
})
export class WeightLossSecretsCategoryDetailPage implements OnInit {
  @ViewChild('slider') slider: Slides;
  
  constructor() { }

  ngOnInit() {
  }

  nextSlide() {
    this.slider.slideNext();
  }

  previousSlide() {

    this.slider.slidePrev();
  }

  onSlideChanged() {
   let x = this.slider.getActiveIndex();
  }
}
