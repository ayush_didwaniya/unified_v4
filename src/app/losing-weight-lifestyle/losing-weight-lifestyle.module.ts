import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LosingWeightLifestylePage } from './losing-weight-lifestyle.page';

const routes: Routes = [
  {
    path: '',
    component: LosingWeightLifestylePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LosingWeightLifestylePage]
})
export class LosingWeightLifestylePageModule {}
