import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LosingWeightLifestylePage } from './losing-weight-lifestyle.page';

describe('LosingWeightLifestylePage', () => {
  let component: LosingWeightLifestylePage;
  let fixture: ComponentFixture<LosingWeightLifestylePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LosingWeightLifestylePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LosingWeightLifestylePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
