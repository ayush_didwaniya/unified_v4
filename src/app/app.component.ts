import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
// import { PromotionsPage } from './promotions/promotions.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Promotions',
      url: '/promotions',
      icon: 'exit'
    },
    {
      title: 'Schedular',
      url: '/schedular',
      icon: 'exit'
    },
    {
      title: 'Body Mass Calculator',
      url: '/body-mass-calculator',
      icon: 'exit'
    },
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Contact Us',
      url: '/contact-us',
      icon: 'contact'
    },
    {
      title: 'Our Bariatric Programs',
      url: '/our-bariatric-programs',
      icon: 'exit'
    },
    {
      title: 'Our Philosophy',
      url: '/our-philosophy',
      icon: 'exit'
    },
    {
      title: 'Clinical Staff',
      url: '/events-category',
      icon: 'exit'
    },
    {
      title: 'Copyright, Trademark An...',
      url: '/copyright-trademark',
      icon: 'exit'
    },
    {
      title: 'Your Weight Control Guide',
      url: '/your-weight-control-guide',
      icon: 'exit'
    },
    {
      title: 'Losing weight is a Lifestyle',
      url: '/losing-weight-lifestyle',
      icon: 'exit'
    },
    {
      title: 'The weight Loss Race',
      url: '/weight-loss-race',
      icon: 'exit'
    },
    {
      title: 'Weight Loss Secrets',
      url: '/weight-loss-secrets',
      icon: 'exit'
    },
    {
      title: 'App Tour',
      url: '/app-tour',
      icon: 'exit'
    },
    {
      title: 'Video Channels',
      url: '/video-channels',
      icon: 'exit'
    },

  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
