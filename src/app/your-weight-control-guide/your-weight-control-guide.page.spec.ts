import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourWeightControlGuidePage } from './your-weight-control-guide.page';

describe('YourWeightControlGuidePage', () => {
  let component: YourWeightControlGuidePage;
  let fixture: ComponentFixture<YourWeightControlGuidePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourWeightControlGuidePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourWeightControlGuidePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
