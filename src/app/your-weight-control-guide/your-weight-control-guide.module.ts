import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { YourWeightControlGuidePage } from './your-weight-control-guide.page';

const routes: Routes = [
  {
    path: '',
    component: YourWeightControlGuidePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [YourWeightControlGuidePage]
})
export class YourWeightControlGuidePageModule {}
