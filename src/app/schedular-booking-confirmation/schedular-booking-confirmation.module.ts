import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SchedularBookingConfirmationPage } from './schedular-booking-confirmation.page';

const routes: Routes = [
  {
    path: '',
    component: SchedularBookingConfirmationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SchedularBookingConfirmationPage]
})
export class SchedularBookingConfirmationPageModule {}
