import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-schedular-ctrl',
  templateUrl: './schedular-ctrl.page.html',
  styleUrls: ['./schedular-ctrl.page.scss'],
})
export class SchedularCtrlPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToPage(Page){
    this.router.navigate([Page]);
  }

}
