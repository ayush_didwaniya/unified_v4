import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SchedularCtrlPage } from './schedular-ctrl.page';

const routes: Routes = [
  {
    path: '',
    component: SchedularCtrlPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SchedularCtrlPage]
})
export class SchedularCtrlPageModule {}
