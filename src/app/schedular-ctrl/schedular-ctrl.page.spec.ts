import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedularCtrlPage } from './schedular-ctrl.page';

describe('SchedularCtrlPage', () => {
  let component: SchedularCtrlPage;
  let fixture: ComponentFixture<SchedularCtrlPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedularCtrlPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedularCtrlPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
