import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { PromotionsDetailsPage } from './promotions-details.page';
import { FooterModule } from '../components/footer/footer.module';

const routes: Routes = [
  {
    path: '',
    component: PromotionsDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FooterModule,
    RouterModule.forChild(routes)
  ],
  // exports:[
  //   PromotionsDetailsPage
  // ],
  declarations: [PromotionsDetailsPage]
})
export class PromotionsDetailsPageModule {}
