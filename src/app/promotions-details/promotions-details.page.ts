import { Component, OnInit, ViewChild } from '@angular/core';
import { Slides } from '@ionic/angular';

@Component({
  selector: 'app-promotions-details',
  templateUrl: './promotions-details.page.html',
  styleUrls: ['./promotions-details.page.scss'],
})
export class PromotionsDetailsPage implements OnInit {
  currentIndex = 0;
  @ViewChild('slider') slider: Slides;
  slides = [
    {
      title: 'Dream\'s Adventure',
      imageUrl: '../../assets/images/a.jpg',
      songs: 2,
      private: false
    },
    {
      title: 'For the Weekend',
      imageUrl: '../../assets/images/a.jpg',
      songs: 4,
      private: false
    },
    {
      title: 'Family Time',
      imageUrl: '../../assets/images/a.jpg',
      songs: 5,
      private: true
    },
    {
      title: 'My Trip',
      imageUrl: '../../assets/images/a.jpg',
      songs: 12,
      private: true
    }
  ];

  constructor() { }

 

    nextSlide() {
      console.log("next");
    this.slider.slideNext();
  }

    previousSlide() {
    
    this.slider.slidePrev();
  }

  onSlideChanged() {
    let x = this.slider.getActiveIndex();
    console.log(x);
  }

  ngOnInit() {
  }


}
