import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { EventsDetailsPage } from './events-details.page';
import { FooterModule } from "../components/footer/footer.module";
const routes: Routes = [
  {
    path: '',
    component: EventsDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FooterModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EventsDetailsPage]
})
export class EventsDetailsPageModule {}
