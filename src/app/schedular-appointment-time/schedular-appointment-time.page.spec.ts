import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedularAppointmentTimePage } from './schedular-appointment-time.page';

describe('SchedularAppointmentTimePage', () => {
  let component: SchedularAppointmentTimePage;
  let fixture: ComponentFixture<SchedularAppointmentTimePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedularAppointmentTimePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedularAppointmentTimePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
