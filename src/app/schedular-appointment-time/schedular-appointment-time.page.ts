import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-schedular-appointment-time',
  templateUrl: './schedular-appointment-time.page.html',
  styleUrls: ['./schedular-appointment-time.page.scss'],
})
export class SchedularAppointmentTimePage implements OnInit {

  constructor(public ModalCtrl: ModalController) { }

  ngOnInit() {
  }

  dismiss(){
    this.ModalCtrl.dismiss();
  }
}
