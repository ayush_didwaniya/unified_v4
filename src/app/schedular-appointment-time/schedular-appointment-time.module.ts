import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SchedularAppointmentTimePage } from './schedular-appointment-time.page';

const routes: Routes = [
  {
    path: '',
    component: SchedularAppointmentTimePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SchedularAppointmentTimePage]
})
export class SchedularAppointmentTimePageModule {}
