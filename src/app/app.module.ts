import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, RouteReuseStrategy, Routes } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HomePageModule } from './home/home.module';
import { ImageCategoryPageModule } from './image-category/image-category.module';
import { ImageItemsPageModule } from './image-items/image-items.module';
import { EventsCategoryPageModule } from './events-category/events-category.module';
import { EventsDetailsPageModule } from './events-details/events-details.module';
import { FooterModule } from './components/footer/footer.module';
import { ImageViewPageModule } from './image-view/image-view.module';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { SchedularTimeSelectPageModule } from './schedular-time-select/schedular-time-select.module';
import { SchedularAppointmentTimePageModule } from './schedular-appointment-time/schedular-appointment-time.module';
import { VideoChannelModalPageModule } from './video-channel-modal/video-channel-modal.module';
// import { PromotionsDetailsPage } from './promotions-details/promotions-details.page';
// import { PromotionsDetailsPageModule } from './promotions-details/promotions-details.module';

// import { FooterComponent } from './footer/footer.component';

// import { HomePageModule } from './home/home.module';
// import { promotion } from './home/home.page';
@NgModule({
  declarations: [AppComponent],
  entryComponents: [
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonicModule,
    HomePageModule,
    ImageCategoryPageModule,
    EventsCategoryPageModule,
    EventsDetailsPageModule,
    ImageItemsPageModule,
    FooterModule,
    ImageViewPageModule,
    SchedularTimeSelectPageModule,
    SchedularAppointmentTimePageModule,
    VideoChannelModalPageModule,
    // PromotionsDetailsPageModule
  ],
  exports: [
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DatePicker,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
