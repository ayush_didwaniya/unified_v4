import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BodyMassCalculatorPage } from './body-mass-calculator.page';

const routes: Routes = [
  {
    path: '',
    component: BodyMassCalculatorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BodyMassCalculatorPage]
})
export class BodyMassCalculatorPageModule {}
