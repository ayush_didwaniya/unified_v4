import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-video-channels',
  templateUrl: './video-channels.page.html',
  styleUrls: ['./video-channels.page.scss'],
})
export class VideoChannelsPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
   goToPage(page){
     this.router.navigate([page]);
   }
}
