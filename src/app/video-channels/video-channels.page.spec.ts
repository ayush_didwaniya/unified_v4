import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoChannelsPage } from './video-channels.page';

describe('VideoChannelsPage', () => {
  let component: VideoChannelsPage;
  let fixture: ComponentFixture<VideoChannelsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoChannelsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoChannelsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
