import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeightLossRacePage } from './weight-loss-race.page';

describe('WeightLossRacePage', () => {
  let component: WeightLossRacePage;
  let fixture: ComponentFixture<WeightLossRacePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeightLossRacePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeightLossRacePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
