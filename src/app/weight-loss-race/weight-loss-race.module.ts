import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WeightLossRacePage } from './weight-loss-race.page';

const routes: Routes = [
  {
    path: '',
    component: WeightLossRacePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WeightLossRacePage]
})
export class WeightLossRacePageModule {}
