import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CopyrightTrademarkPage } from './copyright-trademark.page';

const routes: Routes = [
  {
    path: '',
    component: CopyrightTrademarkPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CopyrightTrademarkPage]
})
export class CopyrightTrademarkPageModule {}
