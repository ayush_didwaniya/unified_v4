import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyrightTrademarkPage } from './copyright-trademark.page';

describe('CopyrightTrademarkPage', () => {
  let component: CopyrightTrademarkPage;
  let fixture: ComponentFixture<CopyrightTrademarkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CopyrightTrademarkPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyrightTrademarkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
