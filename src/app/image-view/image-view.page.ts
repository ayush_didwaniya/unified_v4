import { Component, OnInit } from '@angular/core';
import { ViewController } from '@ionic/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-image-view',
  templateUrl: './image-view.page.html',
  styleUrls: ['./image-view.page.scss'],
})
export class ImageViewPage implements OnInit {

  constructor(public ModalCtrl: ModalController) { }

  ngOnInit() {
  }

  dismiss(){
    this.ModalCtrl.dismiss();
  }
}
