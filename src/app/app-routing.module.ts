import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'home/:id',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  {
    path: 'promotions',
    loadChildren: './promotions/promotions.module#PromotionsPageModule'
  },
  // {
  //   path: 'promotions/promotion-details',
  //   loadChildren: './promotions/promotions.module#PromotionsPageModule'
  // },
  // {
  //   path: 'promotions/promotion-details',
  //   component: './promotions-details/promotions-details.module#PromotionsDetailsPageModule'
  // },
  { 
    path: 'schedular',
    loadChildren: './scheduler/scheduler.module#SchedulerPageModule'
  },
  {
    path: 'contact-us',
    loadChildren: './contact-us/contact-us.module#ContactUsPageModule' 
  },
  { 
    path: 'body-mass-calculator',
    loadChildren: './body-mass-calculator/body-mass-calculator.module#BodyMassCalculatorPageModule' 
  },
  { 
    path: 'image-category',
    loadChildren: './image-category/image-category.module#ImageCategoryPageModule'
  },
  { path: 'image-items',
    loadChildren: './image-items/image-items.module#ImageItemsPageModule' },
  { path: 'events-category', loadChildren: './events-category/events-category.module#EventsCategoryPageModule' },
  { path: 'events-details', loadChildren: './events-details/events-details.module#EventsDetailsPageModule' },
  { path: 'image-view', loadChildren: './image-view/image-view.module#ImageViewPageModule' },
  // { path: 'promotions-details', loadChildren: './promotions-details/promotions-details.module#PromotionsDetailsPageModule' },
  { path: 'schedular-time-select', loadChildren: './schedular-time-select/schedular-time-select.module#SchedularTimeSelectPageModule' },
  { path: 'schedular-appointment-time', loadChildren: './schedular-appointment-time/schedular-appointment-time.module#SchedularAppointmentTimePageModule' },
  { path: 'schedular-ctrl', loadChildren: './schedular-ctrl/schedular-ctrl.module#SchedularCtrlPageModule' },
  { path: 'schedular-booking-summary', loadChildren: './schedular-booking-summary/schedular-booking-summary.module#SchedularBookingSummaryPageModule' },
  { path: 'schedular-booking-confirmation', loadChildren: './schedular-booking-confirmation/schedular-booking-confirmation.module#SchedularBookingConfirmationPageModule' },
  { path: 'video-channels', loadChildren: './video-channels/video-channels.module#VideoChannelsPageModule' },
  { path: 'video-channels-details', loadChildren: './video-channels-details/video-channels-details.module#VideoChannelsDetailsPageModule' },
  { path: 'app-tour', loadChildren: './app-tour/app-tour.module#AppTourPageModule' },
  { path: 'video-channel-modal', loadChildren: './video-channel-modal/video-channel-modal.module#VideoChannelModalPageModule' },
  { path: 'weight-loss-secrets', loadChildren: './weight-loss-secrets/weight-loss-secrets.module#WeightLossSecretsPageModule' },
  { path: 'weight-loss-secrets-detail', loadChildren: './weight-loss-secrets-detail/weight-loss-secrets-detail.module#WeightLossSecretsDetailPageModule' },
  { path: 'our-bariatric-programs', loadChildren: './our-bariatric-programs/our-bariatric-programs.module#OurBariatricProgramsPageModule' },
  { path: 'copyright-trademark', loadChildren: './copyright-trademark/copyright-trademark.module#CopyrightTrademarkPageModule' },
  { path: 'losing-weight-lifestyle', loadChildren: './losing-weight-lifestyle/losing-weight-lifestyle.module#LosingWeightLifestylePageModule' },
  { path: 'weight-loss-race', loadChildren: './weight-loss-race/weight-loss-race.module#WeightLossRacePageModule' },
  { path: 'our-philosophy', loadChildren: './our-philosophy/our-philosophy.module#OurPhilosophyPageModule' },
  { path: 'your-weight-control-guide', loadChildren: './your-weight-control-guide/your-weight-control-guide.module#YourWeightControlGuidePageModule' },
  { path: 'weight-loss-secrets-category-detail', loadChildren: './weight-loss-secrets-category-detail/weight-loss-secrets-category-detail.module#WeightLossSecretsCategoryDetailPageModule' }




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
