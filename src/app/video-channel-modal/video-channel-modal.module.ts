import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VideoChannelModalPage } from './video-channel-modal.page';

const routes: Routes = [
  {
    path: '',
    component: VideoChannelModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VideoChannelModalPage]
})
export class VideoChannelModalPageModule {}
