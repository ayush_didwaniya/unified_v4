import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoChannelModalPage } from './video-channel-modal.page';

describe('VideoChannelModalPage', () => {
  let component: VideoChannelModalPage;
  let fixture: ComponentFixture<VideoChannelModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoChannelModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoChannelModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
