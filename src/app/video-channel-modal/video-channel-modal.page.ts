import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-video-channel-modal',
  templateUrl: './video-channel-modal.page.html',
  styleUrls: ['./video-channel-modal.page.scss'],
})
export class VideoChannelModalPage implements OnInit {

  constructor(private modalCtrl:ModalController) { }

  ngOnInit() {
  }

  closeModal() {
    console.log("close")
    this.modalCtrl.dismiss();
  }
}
