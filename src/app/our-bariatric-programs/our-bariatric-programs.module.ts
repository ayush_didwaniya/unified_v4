import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OurBariatricProgramsPage } from './our-bariatric-programs.page';

const routes: Routes = [
  {
    path: '',
    component: OurBariatricProgramsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OurBariatricProgramsPage]
})
export class OurBariatricProgramsPageModule {}
