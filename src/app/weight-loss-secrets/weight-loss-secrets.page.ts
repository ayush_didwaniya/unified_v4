import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-weight-loss-secrets',
  templateUrl: './weight-loss-secrets.page.html',
  styleUrls: ['./weight-loss-secrets.page.scss'],
})
export class WeightLossSecretsPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToPage(Page){
    this.router.navigate([Page]);
  }
}
