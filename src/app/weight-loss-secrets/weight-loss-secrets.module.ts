import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WeightLossSecretsPage } from './weight-loss-secrets.page';
import { FooterModule } from '../components/footer/footer.module';

const routes: Routes = [
  {
    path: '',
    component: WeightLossSecretsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FooterModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WeightLossSecretsPage]
})
export class WeightLossSecretsPageModule {}
