import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeightLossSecretsPage } from './weight-loss-secrets.page';

describe('WeightLossSecretsPage', () => {
  let component: WeightLossSecretsPage;
  let fixture: ComponentFixture<WeightLossSecretsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeightLossSecretsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeightLossSecretsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
