import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PromotionsPage } from './promotions.page';
import { FooterModule } from '../components/footer/footer.module';
// import { PromotionsDetailsPageModule } from '../promotions-details/promotions-details.module';
import { PromotionsDetailsPage } from '../promotions-details/promotions-details.page';

const routes: Routes = [
  {
    path: '',
    component: PromotionsPage,
    // children: [
    //   {
    //     path: 'promotion-details',
    //     component: PromotionsDetailsPage
    //   }
    // ]
  },
  {
    path: 'promotion-details',
    component: PromotionsDetailsPage,
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FooterModule,
    // PromotionsDetailsPageModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [
    PromotionsDetailsPage
  ],
  declarations: [PromotionsPage, PromotionsDetailsPage]
})
export class PromotionsPageModule { }
