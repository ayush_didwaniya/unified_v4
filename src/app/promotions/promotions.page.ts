import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.page.html',
  styleUrls: ['./promotions.page.scss'],
})
export class PromotionsPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }


  goToPage(Page){
    console.log(Page);
    this.router.navigate([Page]);
  }
}
