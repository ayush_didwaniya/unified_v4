import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Slides } from '@ionic/angular';

@Component({
  selector: 'app-weight-loss-secrets-detail',
  templateUrl: './weight-loss-secrets-detail.page.html',
  styleUrls: ['./weight-loss-secrets-detail.page.scss'],
})
export class WeightLossSecretsDetailPage implements OnInit {
  @ViewChild('slider') slider: Slides;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToPage(Page){
    this.router.navigate([Page]);
  }
  nextSlide() {
    this.slider.slideNext();
  }

  previousSlide() {

    this.slider.slidePrev();
  }

  onSlideChanged() {
   let x = this.slider.getActiveIndex();
  }
}
