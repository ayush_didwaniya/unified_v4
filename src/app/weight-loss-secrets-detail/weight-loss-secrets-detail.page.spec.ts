import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeightLossSecretsDetailPage } from './weight-loss-secrets-detail.page';

describe('WeightLossSecretsDetailPage', () => {
  let component: WeightLossSecretsDetailPage;
  let fixture: ComponentFixture<WeightLossSecretsDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeightLossSecretsDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeightLossSecretsDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
