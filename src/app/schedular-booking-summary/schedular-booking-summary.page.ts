import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-schedular-booking-summary',
  templateUrl: './schedular-booking-summary.page.html',
  styleUrls: ['./schedular-booking-summary.page.scss'],
})
export class SchedularBookingSummaryPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToPage(Page){
    this.router.navigate([Page]); 
  }
}
