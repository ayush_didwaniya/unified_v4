import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedularBookingSummaryPage } from './schedular-booking-summary.page';

describe('SchedularBookingSummaryPage', () => {
  let component: SchedularBookingSummaryPage;
  let fixture: ComponentFixture<SchedularBookingSummaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedularBookingSummaryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedularBookingSummaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
