import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SchedularBookingSummaryPage } from './schedular-booking-summary.page';

const routes: Routes = [
  {
    path: '',
    component: SchedularBookingSummaryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SchedularBookingSummaryPage]
})
export class SchedularBookingSummaryPageModule {}
