import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ImageItemsPage } from './image-items.page';
import { FooterModule } from "../components/footer/footer.module";

const routes: Routes = [
  {
    path: '',
    component: ImageItemsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FooterModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ImageItemsPage]
})

export class ImageItemsPageModule {
}
