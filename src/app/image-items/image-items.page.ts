import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ImageViewPage } from '../image-view/image-view.page';

@Component({
  selector: 'app-image-items',
  templateUrl: './image-items.page.html',
  styleUrls: ['./image-items.page.scss'],
})

export class ImageItemsPage implements OnInit {

  constructor(public modalController: ModalController) {}

  ngOnInit() {
  }
  
  async presentModal() {
    const modal = await this.modalController.create({
      component: ImageViewPage
    });
    return await modal.present();
  }
}
