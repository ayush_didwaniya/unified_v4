import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageItemsPage } from './image-items.page';

describe('ImageItemsPage', () => {
  let component: ImageItemsPage;
  let fixture: ComponentFixture<ImageItemsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageItemsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageItemsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
