import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

declare var google: any;
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.page.html',
  styleUrls: ['./contact-us.page.scss'],
})
export class ContactUsPage implements OnInit {
  segment = "ContactUs";
  map: any;
  marker: any;
  @ViewChild('map') mapElement: ElementRef;

  constructor() { }

  ngOnInit() {
   }

  // ionViewDidLoad(){
  //   this.initMap(new google.maps.LatLng(28.70, 77.10));
  // }

  segmentChanged(segment) {
    console.log('Segment changed', segment);
    segment = "(segment==ContactUs)? ContactForm : ContactUs";
  }

  initMap(location) {
    console.log("hello");
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      center: location,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
  }
  addMarker(location) {
    this.marker = new google.maps.Marker({
      position: location,
      map: this.map
    });
    this.map.setCenter(location);
    // this.showInfoWindow();
  }

  // showInfoWindow() {
  //   let contentString = `<p>${this.historyInfo[0].location}</p>`;
  //   let infowindow = new google.maps.InfoWindow({
  //     content: contentString
  //   });
  //   infowindow.open(this.map, this.marker);
  // }
}
