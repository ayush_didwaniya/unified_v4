import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-image-category',
  templateUrl: './image-category.page.html',
  styleUrls: ['./image-category.page.scss'],
})
export class ImageCategoryPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToPage(Page){
    this.router.navigate([Page]);
  }
}
