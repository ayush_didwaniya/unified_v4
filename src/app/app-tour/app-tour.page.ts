import { Component, OnInit } from '@angular/core';
import { HomePage } from '../home/home.page';
import { Router } from '@angular/router';
@Component({
  selector: 'app-app-tour',
  templateUrl: './app-tour.page.html',
  styleUrls: ['./app-tour.page.scss'],
})
export class AppTourPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToPage(page){
    this.router.navigate([page]);
  }

}
