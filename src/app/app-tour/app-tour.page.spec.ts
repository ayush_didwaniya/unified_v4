import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppTourPage } from './app-tour.page';

describe('AppTourPage', () => {
  let component: AppTourPage;
  let fixture: ComponentFixture<AppTourPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppTourPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppTourPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
