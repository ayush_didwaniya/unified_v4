import { Component, OnInit, ViewChild } from '@angular/core';
import { Slides, ModalController } from '@ionic/angular';
import { VideoChannelModalPage } from '../video-channel-modal/video-channel-modal.page';

@Component({
  selector: 'app-video-channels-details',
  templateUrl: './video-channels-details.page.html',
  styleUrls: ['./video-channels-details.page.scss'],
})
export class VideoChannelsDetailsPage implements OnInit {
  @ViewChild('slider') slider: Slides;

  constructor(public modalController: ModalController) { }

  currentIndex = 0;

  nextSlide() {
    this.slider.slideNext();
  }

  previousSlide() {

    this.slider.slidePrev();
  }

  onSlideChanged() {
   let x = this.slider.getActiveIndex();
  }
  ngOnInit() {
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: VideoChannelModalPage
    });
    return await modal.present();
  }
}