import { Component, OnInit } from '@angular/core';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { ModalController } from '@ionic/angular';
import { SchedularTimeSelectPage } from '../schedular-time-select/schedular-time-select.page';
import { Router } from '@angular/router';
// import { DatePicker } from '@ionic-native/date-picker';
@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.page.html',
  styleUrls: ['./scheduler.page.scss'],
})
export class SchedulerPage implements OnInit {
  Date: any;
  constructor(private datePicker: DatePicker,public modalController: ModalController,private router:Router) { }

  ngOnInit() {
    this.Date= this.dateFormat(new Date());
    }
  
  getDate() {
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        this.Date = this.dateFormat(date)
        console.log("startDtaeTime", this.Date);
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  dateFormat(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var date1 = [year, month, day].join('-');
    return date1;
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: SchedularTimeSelectPage
    });
    return await modal.present();
  }

  goToPage(Page){
    this.router.navigate([Page]);
  }

}
