import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OurPhilosophyPage } from './our-philosophy.page';

describe('OurPhilosophyPage', () => {
  let component: OurPhilosophyPage;
  let fixture: ComponentFixture<OurPhilosophyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OurPhilosophyPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OurPhilosophyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
