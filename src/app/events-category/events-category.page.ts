import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-events-category',
  templateUrl: './events-category.page.html',
  styleUrls: ['./events-category.page.scss'],
})
export class EventsCategoryPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToPage(Page){
    this.router.navigate([Page]);
  }
}
