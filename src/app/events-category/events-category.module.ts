import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EventsCategoryPage } from './events-category.page';
import { FooterModule } from "../components/footer/footer.module";

const routes: Routes = [
  {
    path: '',
    component: EventsCategoryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FooterModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EventsCategoryPage]
})
export class EventsCategoryPageModule {}
