import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SchedularAppointmentTimePage } from '../schedular-appointment-time/schedular-appointment-time.page'
@Component({
  selector: 'app-schedular-time-select',
  templateUrl: './schedular-time-select.page.html',
  styleUrls: ['./schedular-time-select.page.scss'],
})
export class SchedularTimeSelectPage implements OnInit {

  constructor(public ModalCtrl: ModalController) { }

  ngOnInit() {
  }

  dismiss(){
    this.ModalCtrl.dismiss();
  }

  async presentModal() {
    this.ModalCtrl.dismiss();
    const modal = await this.ModalCtrl.create({
      component: SchedularAppointmentTimePage
    });
    return await modal.present();
  }
}
