import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SchedularTimeSelectPage } from './schedular-time-select.page';

const routes: Routes = [
  {
    path: '',
    component: SchedularTimeSelectPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SchedularTimeSelectPage]
})
export class SchedularTimeSelectPageModule {}
