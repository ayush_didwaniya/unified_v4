import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedularTimeSelectPage } from './schedular-time-select.page';

describe('SchedularTimeSelectPage', () => {
  let component: SchedularTimeSelectPage;
  let fixture: ComponentFixture<SchedularTimeSelectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedularTimeSelectPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedularTimeSelectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
